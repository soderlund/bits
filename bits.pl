#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use Carp ();
use Encode ();
use Getopt::Long ();

use HTML::FormatText ();
use HTML::Tree ();
use LWP::UserAgent ();
use URI::file ();
use URI::URL ();

my $ARG_CONTENTTYPE = q(t);
my $ARG_LWP_MAXSIZE = q(m);
my $ARG_LWP_NODNT = q(d);
my $ARG_LWP_NOVERIFYSSL = q(s);
my $ARG_LWP_TIMEOUT = q(w);
my $ARG_LWP_USERAGENT = q(u);
my $ARG_NOINTERACTIVE = q(q);
my $ARG_NOPAGE = q(p);
my $ARG_NOPURGETRASH = q(a);

my $CMD_LINKS_DOWNLOAD = q(d);
my $CMD_LINKS_GO = q(g);
my $CMD_LINKS_PRINT = q(p);
my $CMD_QUIT = q(q);
my $CMD_RETRY = q(r);
my $CMD_WRITE = q(w);

my $ENV_PAGER = q(PAGER);
my $ENV_WWWPAGER = q(WWWPAGER);

my $LWP_PROTOCOLS_DOWNLOAD = [qw(file ftp ftps http https)];
my $LWP_PROTOCOLS_TEXT = [qw(file http https)];
my $LWP_MIMETYPES_DOWNLOAD = [q(*/*)];
my $LWP_MIMETYPES_TEXT = [qw(text/html text/plain)];
my $LWP_USERAGENT = q[bits (1.0; Unix; text)];

my $DOWNLOAD_FILENAME_DEFAULT = q(download);
my $PAGER_DEFAULT = q(more);

my $TTY_COLS_DEFAULT = 80;
my $TTY_COLS_TAB = 8;
my $TTY_SHORTENED = q(…);

my $HAS_READKEY = eval
{
	require Term::ReadKey;
	Term::ReadKey->import();
	return(1);
};

sub arg
{
	my ($opts, $opt) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($opt) && !ref($opt)) or Carp::croak(q(opt));
	return($opts->{$opt}) if exists($opts->{$opt});
	return();
}

sub getopt_args
{
	0 == scalar(@_) or Carp::croak(q(argc));
	return(qq(${ARG_CONTENTTYPE}:s),
			qq(${ARG_LWP_MAXSIZE}:i),
			qq(${ARG_LWP_TIMEOUT}:i),
			qq(${ARG_LWP_USERAGENT}:s),
			$ARG_LWP_NODNT,
			$ARG_LWP_NOVERIFYSSL,
			$ARG_NOINTERACTIVE,
			$ARG_NOPAGE,
			$ARG_NOPURGETRASH);
}

sub arg_contenttype
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_CONTENTTYPE));
}

sub arg_contenttype_html
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(defined(arg_contenttype($opts))
		&& arg_contenttype($opts) =~ m/^text\/html$/aais);
}

sub arg_contenttype_plain
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(defined(arg_contenttype($opts))
		&& arg_contenttype($opts) =~ m/^text\/plain$/aais);
}

sub arg_lwp_maxsize
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_LWP_MAXSIZE));
}

sub arg_lwp_nodnt
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_LWP_NODNT));
}

sub arg_lwp_noverifyssl
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_LWP_NOVERIFYSSL));
}

sub arg_lwp_timeout
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_LWP_TIMEOUT));
}

sub arg_lwp_useragent
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_LWP_USERAGENT));
}

sub arg_nointeractive
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_NOINTERACTIVE));
}

sub arg_nopage
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_NOPAGE));
}

sub arg_nopurgetrash
{
	my ($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(arg($opts, $ARG_NOPURGETRASH));
}

sub slurp_stdin
{
	0 == scalar(@_) or Carp::croak(q(argc));
	my $content = q();
	while (!eof(STDIN))
	{
		my $line = readline(STDIN);
		if (!defined($line))
		{
			warn(qq(Failed to read from STDIN: $!\n));
			return();
		}
		$content .= $line;
	}
	return(Encode::decode_utf8($content, Encode::FB_DEFAULT));
}

sub slurp_file
{
	my ($path) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($path) && !ref($path)) or Carp::croak(q(path));
	my $fh;
	if (!open($fh, q(<), $path))
	{
		warn(qq(Failed to open "$path": $!\n));
		return();
	}
	my $content = q();
	while (!eof($fh))
	{
		my $line = readline($fh);
		if (!defined($line))
		{
			warn(qq(Failed to read from "$path": $!\n));
			close($fh);
			return();
		}
		$content .= $line;
	}
	if (!close($fh))
	{
		warn(qq(Failed to close "$path": $!\n));
		return();
	}
	return(Encode::decode_utf8($content, Encode::FB_DEFAULT));
}

sub lwp_protocols_allowed_download
{
	0 == scalar(@_) or Carp::croak(q(argc));
	(defined($LWP_PROTOCOLS_DOWNLOAD)
		&& q(ARRAY) eq ref($LWP_PROTOCOLS_DOWNLOAD)
		&& scalar(@{$LWP_PROTOCOLS_DOWNLOAD}) > 0)
		or die(qq(Invalid LWP download protocols definition\n));
	return($LWP_PROTOCOLS_DOWNLOAD);
}

sub lwp_protocols_allowed_text
{
	0 == scalar(@_) or Carp::croak(q(argc));
	(defined($LWP_PROTOCOLS_TEXT)
		&& q(ARRAY) eq ref($LWP_PROTOCOLS_TEXT)
		&& scalar(@{$LWP_PROTOCOLS_TEXT}) > 0)
		or die(qq(Invalid LWP text protocols definition\n));
	return($LWP_PROTOCOLS_TEXT);
}

sub lwp_header_accept_download
{
	0 == scalar(@_) or Carp::croak(q(argc));
	(defined($LWP_MIMETYPES_DOWNLOAD)
		&& q(ARRAY) eq ref($LWP_MIMETYPES_DOWNLOAD)
		&& scalar(@{$LWP_MIMETYPES_DOWNLOAD}) > 0)
		or die(qq(Invalid LWP text protocols definition\n));
	return(join(q(, ), @{$LWP_MIMETYPES_DOWNLOAD}));
}

sub lwp_header_accept_text
{
	0 == scalar(@_) or Carp::croak(q(argc));
	(defined($LWP_MIMETYPES_TEXT)
		&& q(ARRAY) eq ref($LWP_MIMETYPES_TEXT)
		&& scalar(@{$LWP_MIMETYPES_TEXT}) > 0)
		or die(qq(Invalid LWP text protocols definition\n));
	return(join(q(, ), @{$LWP_MIMETYPES_TEXT}));
}

sub configure_lwp_user_agent
{
	my ($opts, $ua) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($ua) && ref($ua)) or Carp::croak(q(ua));
	$ua->agent(defined(arg_lwp_useragent($opts))
			? arg_lwp_useragent($opts) : $LWP_USERAGENT);
	$ua->max_size(arg_lwp_maxsize($opts))
			if defined(arg_lwp_maxsize($opts));
	$ua->timeout(arg_lwp_timeout($opts))
			if defined(arg_lwp_timeout($opts));
	$ua->ssl_opts(q(verify_hostname) => 0)
			if arg_lwp_noverifyssl($opts);
	return(1);
}

sub lwp_request_args
{
	my ($opts, $url) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($url) && !ref($url)) or Carp::croak(q(url));
	my @request_args = ();
	push(@request_args, q(Referer), $url);
	push(@request_args, q(DNT), q(1)) unless arg_lwp_nodnt($opts);
	return(@request_args);
}

sub uri_has_protocol
{
	my ($uri) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(!defined($uri) || !ref($uri)) or Carp::croak(q(uri));
	return($uri =~ m/^[a-z]+:\/\//aais);
}

sub add_protocol
{
	my ($url) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(!defined($url) || !ref($url)) or Carp::croak(q(url));
	return(qq(http://$url));
}

sub maybe_add_protocol
{
	my ($url) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(!defined($url) || !ref($url)) or Carp::croak(q(url));
	return(uri_has_protocol($url) ? $url : add_protocol($url));
}

sub download_url
{
	my ($opts, $url, $path) = @_;
	3 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($url) && !ref($url)) or Carp::croak(q(url));
	(defined($path) && !ref($path)) or Carp::croak(q(path));
	my $ua = LWP::UserAgent->new();
	configure_lwp_user_agent($opts, $ua);
	$ua->protocols_allowed(lwp_protocols_allowed_download());
	my @request_args = (q(Accept), lwp_header_accept_download(),
			q(:content_file), $path);
	push(@request_args, lwp_request_args($opts, $url));
	my $response = $ua->get($url, @request_args);
	return($response->headers()->content_length())
			if $response->is_success();
	warn($response->status_line() . qq(\n));
	return();
}

sub slurp_url
{
	my ($opts, $url) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($url) && !ref($url)) or Carp::croak(q(url));
	my $ua = LWP::UserAgent->new();
	configure_lwp_user_agent($opts, $ua);
	$ua->protocols_allowed(lwp_protocols_allowed_text());
	my @request_args = (q(Accept), lwp_header_accept_text());
	push(@request_args, lwp_request_args($opts, $url));
	my $response = $ua->get($url, @request_args);
	if ($response->is_success())
	{
		my $headers = $response->headers();
		if ($headers->content_is_html()
		|| $headers->content_is_text())
		{
			my $is_html = $headers->content_is_html();
			my $raw = $response->decoded_content();
			return($raw, $is_html,
					$response->base()->as_string());
		}
		else
		{
			warn(q(Disallowed MIME type: ")
				. $headers->content_type() . qq("\n));
			return(undef(), undef(), undef());
		}
	}
	else
	{
		warn($response->status_line() . qq(\n));
		return(undef(), undef(), undef());
	}
}

sub base_path
{
	my ($path) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($path) && !ref($path)) or Carp::croak(q(path));
	my $u = URI::file->new_abs($path);
	return($u->as_string());
}

sub slurp_something
{
	my ($opts, $url) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(!defined($url) || !ref($url)) or Carp::croak(q(url));
	if (defined($url))
	{
		return(slurp_url($opts, $url))
				if uri_has_protocol($url);
		return(slurp_file($url), undef(), undef()) if -f $url;
		return(slurp_url($opts, add_protocol($url)));
	}
	else
	{
		return(slurp_stdin(), undef(), undef());
	}
}

sub role_is_trash
{
	my ($element) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	defined($element) or Carp::croak(q(element));
	my $role = $element->attr(q(role));
	return() unless defined($role);
	return($role =~ m/^banner/aais
			|| $role =~ m/^navigation/aais
			|| $role =~ m/^search/aais
			|| $role =~ m/^secondary/aais);
}

sub id_is_trash
{
	my ($element) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	defined($element) or Carp::croak(q(element));
	my $id = $element->id();
	return() unless defined($id);
	return($id =~ m/^banner/aais
			|| $id =~ m/banner$/aais
			|| $id =~ m/^breadcrumb/aais
			|| $id =~ m/breadcrumbs?$/aais
			|| $id =~ m/^comment/aais
			|| $id =~ m/comm$/aais
			|| $id =~ m/comments?$/aais
			|| $id =~ m/^footer/aais
			|| $id =~ m/footer$/aais
			|| $id =~ m/^header/aais
			|| $id =~ m/header$/aais
			|| $id =~ m/^menu/aais
			|| $id =~ m/menu$/aais
			|| $id =~ m/^nav/aais
			|| $id =~ m/nav$/aais
			|| $id =~ m/navigation$/aais
			|| $id =~ m/^search/aais
			|| $id =~ m/search$/aais
			|| $id =~ m/^sidebar/aais
			|| $id =~ m/sidebar$/aais);
}

sub is_trash
{
	my ($element) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	defined($element) or Carp::croak(q(element));
	my %trash_elements = (q(applet) => 1,
			q(area) => 1,
			q(button) => 1,
			q(embed) => 1,
			q(fieldset) => 1,
			q(footer) => 1,
			q(form) => 1,
			q(frame) => 1,
			q(frameset) => 1,
			q(header) => 1,
			q(iframe) => 1,
			q(input) => 1,
			q(label) => 1,
			q(legend) => 1,
			q(map) => 1,
			q(nav) => 1,
			q(object) => 1,
			q(optgroup) => 1,
			q(option) => 1,
			q(param) => 1,
			q(script) => 1,
			q(select) => 1,
			q(style) => 1,
			q(textarea) => 1);
	return(exists($trash_elements{$element->tag()})
			|| (q(div) eq $element->tag()
				&& id_is_trash($element))
			|| role_is_trash($element));
}

sub prune_trash_elements
{
	my ($tree) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	defined($tree) or Carp::croak(q(tree));
	foreach my $element ($tree->content_list())
	{
		next() unless ref($element);
		if (is_trash($element))
		{
			$element->detach();
			$element->delete();
		}
		elsif ($element->tag() eq q(img))
		{
			my $alt = $element->attr(q(alt));
			if (!defined($alt)
			|| length($alt) < 1
			|| $alt eq q(alt))
			{
				$element->detach();
				$element->delete();
			}
			else
			{
				$element->replace_with($alt)->delete();
			}
		}
		else
		{
			prune_trash_elements($element);
		}
	}
	return(1);
}

sub configure_tree
{
	my ($tree) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	defined($tree) or Carp::croak(q(tree));
	$tree->ignore_unknown(0);
	return(1);
}

sub parse_and_prune_tree
{
	my ($opts, $tree, $content) = @_;
	3 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	defined($tree) or Carp::croak(q(tree));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	configure_tree($tree);
	if (!$tree->parse_content($content))
	{
		warn(qq(Failed to parse HTML tree\n));
		return();
	}
	prune_trash_elements($tree) unless arg_nopurgetrash($opts);
	return(1);
}

sub trim_whitespace
{
	my ($plain) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($plain) && !ref($plain)) or Carp::croak(q(plain));
	$plain =~ s/^\s+$//aagim;
	$plain =~ s/^[\*\-\+\s]+$//aagim;
	$plain =~ s/^\s+//aais;
	$plain =~ s/\s+$//aais;
	$plain =~ s/\n\n\n/\n\n/aagis;
	return($plain . qq(\n));
}

sub tty_cols
{
	0 == scalar(@_) or Carp::croak(q(argc));
	return($TTY_COLS_DEFAULT) unless $HAS_READKEY;
	my($w_ch, $h_ch, $w_px, $h_px) =
			Term::ReadKey::GetTerminalSize();
	return($w_ch);
}

sub purge_invalid_bytes
{
	my ($raw) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($raw) && !ref($raw)) or Carp::croak(q(raw));
	my $cooked = $raw;
	$cooked =~ s/[^\t\n[:print:]]//gu;
	return($cooked);
}

sub plainify_content
{
	my ($opts, $content, $is_html) = @_;
	3 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	return($content) unless $is_html;
	my $tree = HTML::Tree->new();
	if (!parse_and_prune_tree($opts, $tree, $content))
	{
		$tree->delete();
		return();
	}
	my $fmt = HTML::FormatText->new(leftmargin => 0,
			rightmargin => tty_cols() - 1);
	my $plain = $fmt->format($tree);
	$tree->delete();
	return() unless defined($plain);
	return(trim_whitespace($plain));
}

sub pager
{
	0 == scalar(@_) or Carp::croak(q(argc));
	return($ENV{$ENV_WWWPAGER}) if exists($ENV{$ENV_WWWPAGER});
	return($ENV{$ENV_PAGER}) if exists($ENV{$ENV_PAGER});
	return($PAGER_DEFAULT);
}

sub page
{
	my ($content, $pager) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	(defined($pager) && !ref($pager)) or Carp::croak(q(pager));
	local $SIG{q(PIPE)} = q(IGNORE);
	my $fh;
	if (!open($fh, q(|-:encoding(UTF-8)), $pager))
	{
		warn(qq(Failed to open pipe "$pager": $!\n));
		return();
	}
	if (!print({$fh} $content))
	{
		warn(qq(Failed to write to pipe "$pager": $!\n));
		close($fh);
		return();
	};
	if (!close($fh))
	{
		warn(qq(Failed to close pipe "$pager": $!\n));
		# Do not return failure because `close()` will fail with
		# broken pipe if the pipe buffer is exceeded when the
		# pager returns (this is why `$SIG{'PIPE'}` is set).
		return(1);
	}
	return(1);
}

sub print_or_page
{
	my ($opts, $content) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	return(page($content, pager())) unless arg_nopage($opts);
	if (!print($content))
	{
		warn(qq(Failed to print to STDOUT: $!\n));
		return();
	}
	return(1);
}

sub is_javascript_link
{
	my ($href) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($href) && !ref($href)) or Carp::croak(q(href));
	return $href =~ m/^javascript/aais
			|| $href =~ m/void\(0\)/aais
			|| $href =~ m/^#$/aais;
}

sub is_mail_link
{
	my ($href) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($href) && !ref($href)) or Carp::croak(q(href));
	return $href =~ m/^mailto:/aais;
}

sub is_relative_anchor_link
{
	my ($href) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($href) && !ref($href)) or Carp::croak(q(href));
	return $href =~ m/^#/aais;
}

sub try_absolute
{
	my ($href, $base) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($href) && !ref($href)) or Carp::croak(q(href));
	(!defined($base) || !ref($base)) or Carp::croak(q(base));
	return($href) unless defined($base);
	my $u = URI::URL->new($href, $base);
	return($u->abs()->as_string());
}

sub find_link_by_href
{
	my ($links, $href) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($links) && q(ARRAY) eq ref($links))
			or Carp::croak(q(links));
	(defined($href) && !ref($href)) or Carp::croak(q(href));
	foreach my $link (@{$links})
	{
		(q(ARRAY) eq ref($link) && 2 == scalar(@{$link}))
				or die(qq(Bad link\n));
		return($link) if (lc($href) eq lc($link->[0]));
	}
	return();
}

sub links
{
	my ($opts, $content, $base, $is_html) = @_;
	4 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	return() unless $is_html;
	my @links = ();
	my $tree = HTML::Tree->new();
	parse_and_prune_tree($opts, $tree, $content);
	foreach my $a ($tree->look_down(q(_tag), q(a)))
	{
		my $href_full = $a->attr(q(href));
		my $text = $a->as_trimmed_text();
		next() if (!defined($href_full)
				|| is_javascript_link($href_full)
				|| is_mail_link($href_full)
				|| is_relative_anchor_link($href_full)
				|| $text =~ m/^\s*$/);
		my $href = $href_full;
		$href =~ s/#[\w-]+$//aais;
		my $href_abs = try_absolute($href, $base);
		my $old = find_link_by_href(\@links, $href_abs);
		if (defined($old))
		{
			(q(ARRAY) eq ref($old) && 2 == scalar(@{$old}))
					or die(qq(Bad link\n));
			if (length($text) > length($old->[1]))
			{
				$old->[1] = $text;
			}
		}
		else
		{
			push(@links, [$href_abs, $text]);
		}
	}
	$tree->delete();
	return(\@links);
}

sub maybe_shorten_linkhref
{
	my ($text) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($text) && !ref($text)) or Carp::croak(q(text));
	my $tty_cols_max = tty_cols() - $TTY_COLS_TAB;
	return($text) if length($text) <= $tty_cols_max;
	my $chars_end = $tty_cols_max / 2;
	my $chars_begin = $tty_cols_max - $chars_end;
	return(substr($text, 0, $chars_begin) . $TTY_SHORTENED
			. substr($text, -($chars_end - 1)));
}

sub maybe_shorten_linktext
{
	my ($text) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($text) && !ref($text)) or Carp::croak(q(text));
	my $tty_cols_max = tty_cols() - $TTY_COLS_TAB;
	return(length($text) > $tty_cols_max
		? substr($text, 0, $tty_cols_max - 1) . $TTY_SHORTENED
		: $text);
}

sub right_align_linktext
{
	my ($text) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($text) && !ref($text)) or Carp::croak(q(text));
	my $tty_cols_max = tty_cols() - $TTY_COLS_TAB;
	return($text) if length($text) >= $tty_cols_max;
	my $fill = q();
	for (my $i = 0; $i < $tty_cols_max - length($text); ++$i)
	{
		$fill .= q( );
	}
	return($fill . $text);
}

sub msg_links
{
	my ($links) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(!defined($links) || q(ARRAY) eq ref($links))
			or Carp::croak(q(links));
	return(qq(Not HTML\n)) unless defined($links);
	return(qq(No links\n)) unless scalar(@{$links}) > 0;
	my $txt = q();
	for (my $i = 0; $i < scalar(@{$links}); ++$i)
	{
		my $link = $links->[$i];
		(q(ARRAY) eq ref($link) && 2 == scalar(@{$link}))
				or die(qq(Bad link ($i)\n));
		my $href = right_align_linktext
				(maybe_shorten_linkhref($link->[0]));
		my $text = maybe_shorten_linktext($link->[1]);
		$txt .= ($i + 1) . qq(\t$text\n\t$href\n);
	}
	return($txt);
}

sub find_link
{
	my ($links, $num) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(!defined($links) || q(ARRAY) eq ref($links))
			or Carp::croak(q(links));
	(defined($num) && !ref($num)) or Carp::croak(q(num));
	my $index = $num - 1;
	return() unless defined($links)
			&& $index >= 0 && $index < scalar(@{$links});
	return($links->[$index]);
}

sub last_url_segment
{
	my ($url) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($url) && !ref($url)) or Carp::croak(q(url));
	my $u = URI::URL->new($url);
	my @segments = $u->path_segments();
	return() unless scalar(@segments) > 1;
	my $segment = $segments[-1];
	$segment =~ s/%([0-9A-Fa-f]{2})/chr(hex($1))/eg;
	return(length($segment) > 0 ? $segment : undef())
}

sub guess_filename
{
	my ($url, $fallback) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($url) && !ref($url)) or Carp::croak(q(url));
	(defined($fallback) && !ref($fallback))
			or Carp::croak(q(fallback));
	my $filename = last_url_segment($url);
	return(defined($filename) ? $filename : $fallback);
}

sub write_content
{
	my ($content, $path) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	(defined($path) && !ref($path)) or Carp::croak(q(path));
	my $fh;
	if (!open($fh, q(>:encoding(UTF-8)), $path))
	{
		warn(qq(Failed to open "$path": $!\n));
		return();
	}
	my $success = print({$fh} $content);
	if (!$success)
	{
		warn(qq(Failed to write to "$path": $!\n));
	}
	if (!close($fh))
	{
		warn(qq(Failed to close "$path": $!\n));
		$success = undef();
	}
	return() unless $success;
	return(1);
}

sub looks_like_html
{
	my ($content) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	return($content =~ m/^\s*<!doctype\shtml.*>/aais);
}

sub determine_is_html
{
	my ($opts, $content, $is_html) = @_;
	3 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	return(1) if arg_contenttype_html($opts);
	return() if arg_contenttype_plain($opts);
	return($is_html) if defined($is_html);
	return(looks_like_html($content));
}

sub guess_base
{
	my ($opts, $content, $url) = @_;
	3 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	(!defined($url) || !ref($url)) or Carp::croak(q(url));
	my $tree = HTML::Tree->new();
	if (!parse_and_prune_tree($opts, $tree, $content))
	{
		$tree->delete();
		return();
	}
	my $base = $tree->look_down(q(_tag), q(base));
	my $base_href = defined($base) ? $base->attr(q(href)) : undef();
	$tree->delete();
	return($base_href) if defined($base_href);
	return(base_path($url)) if defined($url);
	return();
}

sub getopts
{
	my ($argv) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($argv) && q(ARRAY) eq ref($argv))
			or Carp::croak(q(argv));
	my %opts = ();
	my @rem = @{$argv};
	Getopt::Long::Configure(q(bundling));
	Getopt::Long::GetOptionsFromArray
			(\@rem, \%opts, getopt_args()) or return();
	if (scalar(@rem) > 1)
	{
		warn(q(Unknown option: ") . $rem[1] . qq("\n));
		return(undef(), undef());
	}
	if (!(!defined(arg_contenttype(\%opts))
			|| arg_contenttype_html(\%opts)
			|| arg_contenttype_plain(\%opts)))
	{
		warn(q(Unknown Content-Type: ")
				. arg_contenttype(\%opts) . qq("\n));
		return(undef(), undef());
	}
	if (!(!defined(arg_lwp_maxsize(\%opts))
			|| arg_lwp_maxsize(\%opts) > 0))
	{
		warn(qq(max_size must be > 0\n));
		return(undef(), undef());
	}
	if (!(!defined(arg_lwp_timeout(\%opts))
			|| arg_lwp_timeout(\%opts) > 0))
	{
		warn(qq(timeout must be > 0\n));
		return(undef(), undef());
	}
	return(\%opts, \@rem);
}

sub command_loop
{
	my ($opts, $cooked, $plain, $base, $is_html, $url) = @_;
	6 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(!defined($cooked) || !ref($cooked)) or Carp::croak(q(cooked));
	(!defined($plain) || !ref($plain)) or Carp::croak(q(plain));
	(!defined($base) || !ref($base)) or Carp::croak(q(base));
	(!defined($is_html) || !ref($is_html))
			or Carp::croak(q(is_html));
	(!defined($url) || !ref($url)) or Carp::croak(q(url));
	while (!eof(STDIN))
	{
		my $input = readline(STDIN);
		if (!defined($input))
		{
			warn(qq(Failed to read STDIN: $!\n));
			return(undef(), undef());
		}
		chomp($input);
		if (($input =~
		qr/^\s*${CMD_LINKS_DOWNLOAD}\s+(\d+)\s+(.+?)\s*$/aas
		|| $input =~
		qr/^\s*${CMD_LINKS_DOWNLOAD}\s+(\d+)\s*$/aas)
		&& defined($plain))
		{
			my $num = $1;
			my $link = find_link(links($opts, $cooked,
					$base, $is_html), $num);
			if (!defined($link))
			{
				warn(qq(Link not found\n));
				next();
			}
			(q(ARRAY) eq ref($link)
					&& 2 == scalar(@{$link}))
					or die(qq(Bad link\n));
			my $path = defined($2) ? $2
				: guess_filename($link->[0], $num);
			if (!defined($2) && -e $path)
			{
				warn(qq(Not clobbering: $path\n));
				next();
			}
			my $bytes = download_url($opts,
				maybe_add_protocol($link->[0]), $path);
			print(qq($bytes) . (defined($2) ? q()
					: qq(: $path)) . qq(\n))
					if defined($bytes);
		}
		elsif ($input =~
		qr/^\s*${CMD_LINKS_DOWNLOAD}\s+(.+?)\s+(.+?)\s*$/aas
		|| $input =~
		qr/^\s*${CMD_LINKS_DOWNLOAD}\s+(.+?)\s*$/aas)
		{
			my $url2 = $1;
			my $path = defined($2) ? $2 : guess_filename
				($url2, $DOWNLOAD_FILENAME_DEFAULT);
			if (!defined($2) && -e $path)
			{
				warn(qq(Not clobbering: $path\n));
				next();
			}
			my $bytes = download_url($opts,
				maybe_add_protocol($url2), $path);
			print(qq($bytes) . (defined($2) ? q()
					: qq(: $path)) . qq(\n))
					if defined($bytes);
		}
		elsif (($input =~ qr/^\s*${CMD_LINKS_GO}\s+(\d+)\s*$/aas
				|| $input =~ m/^\s*(\d+)\s*$/aas)
				&& defined($plain))
		{
			my $num = $1;
			my $link = find_link(links($opts, $cooked,
					$base, $is_html), $num);
			if (!defined($link))
			{
				warn(qq(Link not found\n));
				next();
			}
			(q(ARRAY) eq ref($link)
					&& 2 == scalar(@{$link}))
					or die(qq(Bad link\n));
			return(1, $link->[0]);
		}
		elsif ($input =~ qr/^\s*${CMD_LINKS_GO}\s+(.+?)\s*$/aas)
		{
			return(1, $1);
		}
		elsif ($input =~ qr/^\s*${CMD_LINKS_PRINT}/aas
				&& defined($plain))
		{
			my $linktxt = msg_links(links($opts, $cooked,
					$base, $is_html));
			return(undef(), undef()) unless
					print_or_page($opts, $linktxt);
		}
		elsif ($input =~ qr/^\s*${CMD_QUIT}/aas)
		{
			return(1, undef());
		}
		elsif ($input =~ qr/^\s*${CMD_RETRY}/aas
				&& defined($url))
		{
			return(1, $url);
		}
		elsif ($input =~ qr/^\s*${CMD_WRITE}\s+(.+?)\s*$/aas
				&& defined($plain))
		{
			my $path = $1;
			print(length($cooked) . qq(\n)) if
					write_content($cooked, $path);
		}
		else
		{
			warn(qq(?\n));
		}
	}
	return(1, undef());
}

sub slurp_loop
{
	my ($opts, $url) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(!defined($url) || !ref($url)) or Carp::croak(q(url));
	do
	{
		my ($raw, $is_html, $base) =
				slurp_something($opts, $url);
		my $cooked = defined($raw)
				? purge_invalid_bytes($raw) : q();
		undef($raw);
		$base = defined($base) ? $base
				: guess_base($opts, $cooked, $url);
		$is_html = defined($cooked)
			? determine_is_html($opts, $cooked, $is_html)
			: $is_html;
		my $plain = defined($cooked)
			? plainify_content($opts, $cooked, $is_html)
			: undef();
		if (defined($plain))
		{
			return() if (!print_or_page($opts, $plain)
					&& arg_nointeractive($opts));
		}
		return(1) if arg_nointeractive($opts);
		my ($success, $url_next) = command_loop($opts, $cooked,
				$plain, $base, $is_html, $url);
		return() unless $success;
		$url = $url_next;
	}
	while (defined($url));
	return(1);
}

sub main
{
	my ($argv) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($argv) && q(ARRAY) eq ref($argv))
			or Carp::croak(q(argv));
	my ($opts, $rem) = getopts($argv);
	return() if (!defined($opts) || !defined($rem));
	(defined($opts) && q(HASH) eq ref($opts)) or die(qq(opts\n));
	(defined($rem) && q(ARRAY) eq ref($rem)) or die(qq(rem\n));
	my $url = scalar(@{$rem}) > 0 ? $rem->[0] : undef();
	binmode(STDERR, q[:encoding(UTF-8)]) or die(qq($!\n));
	binmode(STDOUT, q[:encoding(UTF-8)]) or die(qq($!\n));
	return(slurp_loop($opts, $url));
}

exit(main(\@ARGV) ? 0 : 1);

__END__

=pod

=encoding utf8

=head1 Name

bits - Browse in the Shell

=head1 Synopsis

bits [-adpqs] [-m INT] [-t STR] [-u STR] [-w INT] [URL]

=head1 Description

bits is the only web browser that doesn't make you vomit when you see
the sorry state of the web. This is how bits works:

=over

=item 1. Fetch the content from a remote address (or read it from disk).

=item 2. Convert the HTML to plain text.

=item 3. Pipe the plain text to your pager.

=item 4. Read commands from STDIN.

=back

bits is spartan and intentionally does not support sessions, forms,
history, caching or other bloat.

=head1 Options

=over

=item -a: Do not remove trash elements when parsing the HTML.

=item -d: Do not send the Do Not Track header ("DNT: 1"), which is sent
	by default.

=item -m INT: Maximum size (in bytes) to accept in the HTTP response.
	The default is the LWP default (accept any size).

=item -p: Do not page the content. Just print everything to STDOUT.

=item -q: Quit immediately after outputting the document.

=item -s: Disable SSL hostname verification in LWP.

=item -t STR: Force a Content-Type. Valid values are "text/html" and
	"text/plain". The default is to look at the HTTP response or
	guess.

=item -u STR: Set the user agent sent by LWP. Default:
	"bits (1.0; Unix; text)"

=item -w INT: Request timeout (in seconds). The default is the LWP
	default (180 seconds).

=back

=head1 Environment

=over

=item WWWPAGER: Primary pager.

=item PAGER: Secondary pager.

=back

=head1 Commands

=over

=item d INT STR: Download the link with number N to a file system path.

=item d STR STR: Download the given URL to a file system path.

=item g INT: Go to the link with number N.

=item g STR: Go to the given URL.

=item p: Print and number all links.

=item q: Quit.

=item r: Retry fetching (or reading) the document.

=item w STR: Write the current document source to a file system path.

=back

You can omit the file system path in the "d" command. If so, bits tries
to guess the filename by looking at the URL, and falls back to using
the link number or the string "download". Files are only clobbered if
you specify the file system path.

If you specify more than one string argument, then only the last one can
contain white space. All leading and trailing white space is trimmed.
String arguments can not be quoted.

Only specifying a number is interpreted as the "g INT" command.

=head1 URL guessing

If an URL is given as invocation parameter, and it starts with a scheme,
then bits tries to fetch it with LWP. If it does not begin with that
pattern, and the URL is a path to a file that exists in the file system,
then bits tries to read that file. If the path does not exist in the
file system, then bits assumes that you omitted the scheme and that it
is a remote HTTP address. bits reads from STDIN if the URL is omitted
(note that reading from STDIN will cause the command loop to be skipped
because STDIN will have reached EOF).

=head1 Content-Type guessing

When fetching content from a remote address, bits looks at the
Content-Type in the HTTP response to determine if it's HTML or text.
When reading content from disk or STDIN, bits looks at the DOCTYPE
declaration to determine if it's HTML. This behavior can be overridden
by forcing a Content-Type.

Note that bits does not accept any other Content-Types than "text/html"
and "text/plain" in the HTTP request to prevent downloading of images,
PDFs and other annoying formats. Even if the server ignores the Accept
header, bits will still not display any other content than text or HTML.

=head1 Terminal size guessing

bits formats the plain text to fit your terminal width. Long links are
truncated before printing. bits will correctly identify the number of
columns in your terminal if you have Term::ReadKey; otherwise it assumes
that the terminal is 80 columns wide.

=head1 Links

All links are numbered when you print them. The indices are used to
refer to the links with other commands. Duplicates, mailto links and
broken Javascript links are removed before printing. Fragments are
ignored when determining if links are duplicates.

While it is impossible to follow PDF links (because everything else than
HTML and text is blocked), you can still download them, so the link
extraction doesn't filter anything based on file extensions. All
relative links are resolved against the current base URL, if there is
one.

=head1 Protocols

FILE, HTTP and HTTPS are allowed when fetching HTML or text content. FTP
and FTPS are also allowed when using the "d" command.

The "d" and "g" commands are slightly different. "d" always downloads
files with LWP. "g" can fetch content with LWP or by reading it directly
from the file system or STDIN. When reading content from the file
system, LWP is only used if the FILE protocol is specified. LWP is not
used when reading from a file system path without the FILE scheme or
STDIN.

=head1 Encoding

All content that is written to STDOUT, pagers or disk with the "w"
command is converted to UTF-8 regardless of the original encoding.
Furthermore, all bytes that are not printable Unicode characters, tabs
or newlines are removed. This does not apply to the "d" command.

All content that is read from STDIN or disk is assumed to be UTF-8
encoded. Use iconv(1) to get rid of idiot encodings. Content fetched
from remote addresses with LWP is converted from whatever it is by LWP
(and then to UTF-8 when written out, as described above).

=head1 Paging

bits uses the pager indicated by WWWPAGER or PAGER, or more(1) if
neither is set. This pager is used when printing the document and links.

=head1 Trash removal

Nearly all modern websites embed the actual content that people are
interested in in a huge pile of bloated trash. bits has rudimentary
support for removing the most obnoxious crap. Trash removal is done
before rendering the HTML page as text and extracting links.

Removing trash elements is done by traversing the DOM tree and pruning
certain HTML elements. Examples of elements that are removed:

=over

=item Forms.

=item Frames.

=item Headers and footers.

=item Images that lack alt texts.

=item Navigation blocks (many bloated websites have enormous navigation
	blocks where every link is a list item, which sometimes takes up
	hundreds of pages in normal web browsers like Lynx).

=item Stylesheets and embedded executable code like applets, scripts and
	objects (they are not loaded anyway).

=item Elements with IDs or roles that imply they are any of the above or
	sidebars, menus, comment sections, et c.

=back

Some web technologies wrap the entire content in a huge form and it is
therefore removed. Although that is not intentional, it is considered to
be an amazing feature that correctly identifies content that is probably
a waste of time by looking at the presentation.

=head1 Exit status

0 on success, 1 on any failure.

If you use the -q invocation parameter and there is any error fetching,
parsing or paging the content, then bits indicates failure on exit. If
you do not use the -q invocation parameter, then such errors are not
fatal (and therefore do not affect the exit status) and are just printed
to STDERR before the command loop.

=head1 Dependencies

=over

=item HTML::FormatText

=item LWP::Protocol::https

=item LWP::UserAgent

=item URI::file

=item Term::ReadKey (optional)

=back

=head1 Bugs and limitations

Web developers are at the bottom of the barrel in computer science. Most
of them do not understand the code that they copy and paste from search
engines and can not even write valid HTML. Unfortunately they built most
of the websites on the internet.

bits tries to make the best of the situation with the trash removal
feature, but it is prone to removing too much or too little because it
is very crude and treats all websites the same way. Future development
should be aimed at making the trash removal more accurate without
bloating the program or making it specific to certain websites.

bits does not send the default LWP user agent because the web developer
cargo cult thinks security will come to them if they block LWP by user
agent filtering. So bits fakes the user agent in order to not be blocked
by almost every website.

bits does not work with taint mode because it uses $ENV{'WWWPAGER'},
$ENV{'PAGER'} and (implicitly) $ENV{'PATH'} to find your pager.

HTML::FormatText does strange things with PRE elements.

=head1 See also

more(1), surfraw(1), lynx(1), ftp(1), wget(1), HTML::FormatText(1),
LWP::UserAgent(1)

=head1 Homepage

tafl.se/bits

=head1 Author

Alexander Söderlund (soderlund at tafl dot se)

=head1 Copyright

Copyright © 2017-2018 Alexander Söderlund (Sweden)

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

=cut

