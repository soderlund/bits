.POSIX:

PRGNAM = bits
VERSION = 181110

LINT = perlcritic
LINTFLAGS = --noprofile --nocolor --severity 1\
	--exclude ProhibitCascadingIfElse\
	--exclude ProhibitCStyleForLoops\
	--exclude ProhibitEnumeratedClasses\
	--exclude ProhibitEscapedMetaCharacters\
	--exclude ProhibitExcessComplexity\
	--exclude ProhibitMagicNumbers\
	--exclude ProhibitManyArgs\
	--exclude ProhibitNegativeExpressionsInUnlessAndUntilConditions\
	--exclude ProhibitParensWithBuiltins\
	--exclude ProhibitPostfixControls\
	--exclude ProhibitPunctuationVars\
	--exclude RequireArgUnpacking\
	--exclude RequireBriefOpen\
	--exclude RequireCheckedClose\
	--exclude RequireCheckedSyscalls\
	--exclude RequireDotMatchAnything\
	--exclude RequireExtendedFormatting\
	--exclude RequireLineBoundaryMatching\
	--exclude RequirePodSections\
	--exclude RequireTidyCode\
	--exclude RequireTrailingCommas\
	--exclude RequireVersionVar

BIN = ./$(PRGNAM).pl

all:

clean:

mrproper: clean

dist: mrproper
	mkdir '$(PRGNAM)-$(VERSION)'
	find . -maxdepth 1 -mindepth 1\
		-and -not -name '$(PRGNAM)-$(VERSION)'\
		-and -not -name '.git'\
		-exec cp -r '{}' '$(PRGNAM)-$(VERSION)' \;
	tar -cf '$(PRGNAM)-$(VERSION).tar' '$(PRGNAM)-$(VERSION)'
	gzip -9 '$(PRGNAM)-$(VERSION).tar'
	rm -r '$(PRGNAM)-$(VERSION)'

lint:
	$(LINT) $(LINTFLAGS) $(BIN)

